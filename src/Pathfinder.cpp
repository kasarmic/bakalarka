#include "Mapper.h"
#include "Grid_cell.h"
#include "Pathfinder.h"
#include <geodesy/utm.h>
#include <cmath>
#include <list>

void Pathfinder::setup_pathfinder(int num_of_cells, ros::NodeHandle &nodehandle)
{
  start = true;
  current_target_set = false;
  global_target_set = false;
  find_new_path = false;

  grid_size = 0;
  Map_centre_x = 0;
  Map_centre_y = 0;

  num_of_grid_cells = num_of_cells;
  time_of_last_pathfinding = ros::Time::now();

  nodes = new a_star_node *[num_of_grid_cells];
  for (int i = 0; i < num_of_grid_cells; i++)
  {
    nodes[i] = new a_star_node[num_of_grid_cells];
  }

  sub_target_gps = nodehandle.subscribe("/ciel", 1, &Pathfinder::callback_target_gps, this);
  sub_current_gps = nodehandle.subscribe("/navsat/fix", 1, &Pathfinder::callback_current_gps, this);
  path_odom = nodehandle.advertise<sensor_msgs::PointCloud2>("/path", 1);
  path_msg = nodehandle.advertise<nav_msgs::Path>("/nav_msg_path", 1);
}

void Pathfinder::callback_target_gps(const sensor_msgs::NavSatFixConstPtr &msg)
{
  global_target_xyz.x = msg->latitude;
  global_target_xyz.y = msg->longitude;
  global_target_xyz.z = msg->altitude;
  current_target_set = false;
  global_target_set = true;
  pcl::PointCloud<pcl::PointXYZ> empty_path;
  current_path = empty_path;
  find_new_path = true;
  // printf("NOVY CIEL \n");
}

void Pathfinder::callback_current_gps(const sensor_msgs::NavSatFixConstPtr &msg)
{
  geometry_msgs::Point current_position_ECEF;
  if (start)
  {
    start_position.latitude = msg->latitude;
    start_position.longitude = msg->longitude;
    start_position.altitude = msg->altitude;
    start = false;
  }
  else
  {
    current_position_xyz = GPS_to_ENU(msg->latitude, msg->longitude, msg->altitude);

    /*printf("GPS_POS   X: %f Y: %f \n", current_position_xyz.x, current_position_xyz.y);
    printf("SLAM_POS   X: %f Y: %f \n", robot_x, robot_y);*/
  }
}

geometry_msgs::Point Pathfinder::GPS_to_ENU(double latitude, double longitude, double altitude)
{
  // phi,lam,h == latitude, longitude, altitude

  double a = 6378137;
  double b = 6356752.3142;
  double e2 = 1 - (b * b) / (a * a);

  double phi = start_position.latitude * M_PI / 180;
  double lam = start_position.longitude * M_PI / 180;
  double h = start_position.altitude;

  double dphi = latitude * M_PI / 180 - phi;
  double dlam = longitude * M_PI / 180 - lam;
  double dh = altitude - start_position.altitude;

  double tmp1 = sqrt(1 - e2 * sin(phi) * sin(phi));
  double cl = cos(lam);
  double sl = sin(lam);
  double cp = cos(phi);
  double sp = sin(phi);

  geometry_msgs::Point ENU;

  ENU.y = -((a / tmp1 + h) * cp * dlam - (a * (1 - e2) / (tmp1 * tmp1 * tmp1) + h) * sp * dphi * dlam + cp * dlam * dh);
  ENU.x = (a * (1 - e2) / (tmp1 * tmp1 * tmp1) + h) * dphi + 1.5 * cp * sp * a * e2 * dphi * dphi + sp * sp * dh * dphi + 0.5 * sp * cp * (a / tmp1 - h) * dlam * dlam;
  ENU.z = dh - 0.5 * (a - 1.5 * a * e2 * cp * cp + 0.5 * a * e2 + h) * dphi * dphi - 0.5 * cp * cp * (a / tmp1 - h) * dlam * dlam;

  return ENU;
}

void Pathfinder::parse_parameters(Grid_cell **map, float robot_position_x, float robot_position_y, float grid_len, float centre_x, float centre_y)
{
  grid = map;
  grid_size = grid_len;
  Map_centre_x = centre_x;
  Map_centre_y = centre_y;
  for (int x = 0; x < num_of_grid_cells; x++)
  {
    for (int y = 0; y < num_of_grid_cells; y++)
    {
      nodes[x][y].x = x;
      nodes[x][y].y = y;
      nodes[x][y].visited = false;
      nodes[x][y].parent = nullptr;
      nodes[x][y].GlobalGoal = INFINITY;
      nodes[x][y].LocalGoal = INFINITY;
      nodes[x][y].obstacle = !map[x][y].possible_to_traverse;
      nodes[x][y].cell_value_for_pathfinding = map[x][y].cell_value_for_pathfinding+1;
      nodes[x][y].parent_direction = -1;
    }
  }

  for (int i = 0; i < current_path.points.size(); i++)
  {
    int x = get_grid_coord_x(current_path.points[i].x);
    int y = get_grid_coord_y(current_path.points[i].y);
 
    if (i < 20 && x >= 0 && x < num_of_grid_cells && y >= 0 && y < num_of_grid_cells)
    {
      nodes[x][y].cell_value_for_pathfinding = nodes[x][y].cell_value_for_pathfinding-1;
    }
  }

  robot_x = robot_position_x;
  robot_y = robot_position_y;
}

void Pathfinder::take_care_of_path()
{

  if (check_for_target_proximity())
  {
    current_target_set = false;
    find_new_path = true;
  }
  bool check = false;
  if (!check_path() || find_new_path)
  {
    if (global_target_set)
    {
      printf("Finding path global\n");
      printf("%d\n",current_path.points.size());
      printf("%d %d\n",global_target_set,current_target_set);
      find_new_path = false;
      time_of_last_pathfinding = ros::Time::now();
      check = true;
      find_path();
      
    }
  }
  
  if (current_target_set && !check)
  {
    if (time_of_last_pathfinding + ros::Duration(5) < ros::Time::now())
    {
      printf("Finding path after time\n");
      time_of_last_pathfinding = ros::Time::now();
      find_path();
    }
  }
  /*if (!check_path() || find_new_path)
  {
    if (global_target_set)
    {
      find_new_path = false;
      time_of_last_pathfinding = ros::Time::now();
      find_path();
    }
  }*/
  /*if (current_target_set)
  {
    if (time_of_last_pathfinding + ros::Duration(3) > ros::Time::now())
    {
     time_of_last_pathfinding = ros::Time::now();
     current_target_set = false;
     find_path();
    }
  }*/
}
bool Pathfinder::check_path()
{
  // printf("%d \n", current_path.points.size());
  // printf("%d \n", global_target_set);
  if (current_path.points.size() == 0)
  {
    return false;
  }
  else
  {
    for (int i = 5; i < current_path.points.size() - 1; i++)
    {
      int x = get_grid_coord_x(current_path.points[i].x);
      int y = get_grid_coord_y(current_path.points[i].y);
      if (x < 0 && x > num_of_grid_cells && y < 0 && y > num_of_grid_cells)
      {
        return false;
      }
      if (!grid[x][y].possible_to_traverse)
      {
        return false;
      }
    }
  }
  return true;
}

void Pathfinder::find_path()
{
  auto heuristic = [](a_star_node *a, a_star_node *b)
  {
    // return abs(std::max(a->x-b->x,a->y-b->y));
    return sqrt((a->x - b->x) * (a->x - b->x) + (a->y - b->y) * (a->y - b->y));
  };

  int start_x = get_grid_coord_x(robot_x);
  int start_y = get_grid_coord_y(robot_y);
  int end_x = get_grid_coord_x(global_target_xyz.x);
  int end_y = get_grid_coord_y(global_target_xyz.y);
  a_star_node *start_node = &nodes[start_x][start_y];
  a_star_node node;
  a_star_node *end_node = &node;

  if (end_x >= 0 && end_x < num_of_grid_cells && end_y >= 0 && end_y < num_of_grid_cells)
  {
    if (!nodes[end_x][end_y].obstacle)
    {
      current_target_set = false;
    }
  }

  if (current_target_set)
  {
    end_x = get_grid_coord_x(current_target_xyz.x);
    end_y = get_grid_coord_y(current_target_xyz.y);
  }

  end_node->x = end_x;
  end_node->y = end_y;
  a_star_node *closest_node = NULL;
  a_star_node *closest_node_fog = NULL;
  start_node->LocalGoal = 0;
  start_node->GlobalGoal = heuristic(start_node, end_node);

  a_star_node *current_node = start_node;
  std::list<a_star_node *> Nodes_to_test;
  Nodes_to_test.push_back(start_node);

  bool dumb_check = false;
  ros::Time start = ros::Time::now();
  while (!Nodes_to_test.empty() && start + ros::Duration(1.5) > ros::Time::now() && !(current_node->x == end_x && current_node->y == end_y))
  {
    Nodes_to_test.sort([](const a_star_node *a, const a_star_node *b)
                       { return a->GlobalGoal < b->GlobalGoal; });

    while (!Nodes_to_test.empty() && Nodes_to_test.front()->visited)
    {
      Nodes_to_test.pop_front();
    }

    if (Nodes_to_test.empty())
    {
      break;
    }

    current_node = Nodes_to_test.front();
    current_node->visited = true;

    for (int i = -1; i < 2; i++)
    {
      for (int j = -1; j < 2; j++)
      {

        if (current_node->x + i >= 0 && current_node->x + i < num_of_grid_cells && current_node->y + j >= 0 && current_node->y + j < num_of_grid_cells)
        {
          a_star_node *neighbour = &nodes[current_node->x + i][current_node->y + j];

          if (!neighbour->visited && neighbour->obstacle == false)
          {
            Nodes_to_test.push_back(neighbour);
          }

          float temp_goal = current_node->LocalGoal + neighbour->cell_value_for_pathfinding + heuristic(current_node, neighbour);

          if (temp_goal < neighbour->LocalGoal)
          {
            neighbour->parent = current_node;
            neighbour->LocalGoal = temp_goal;
            neighbour->GlobalGoal = neighbour->LocalGoal + heuristic(neighbour, end_node);

            if (node_proximity_check(neighbour, nodes))
            {
              if (closest_node_fog == NULL)
              {
                closest_node_fog = neighbour;
              }
              else
              {
                if (heuristic(neighbour, end_node) < heuristic(closest_node_fog, end_node))
                {
                  dumb_check = true;
                  closest_node_fog = neighbour;
                }
              }
            }
            else
            {
              if (closest_node == NULL)
              {
                closest_node = neighbour;
              }
              else
              {
                if (heuristic(neighbour, end_node) < heuristic(closest_node, end_node))
                {
                  closest_node = neighbour;
                }
              }
            }
          }
        }
      }
    }
  }

  if (closest_node_fog != NULL && &nodes[end_x][end_y] != closest_node && dumb_check)
  {
    current_node = closest_node_fog;
  }
  else
  {
    current_node = closest_node;
  }

  if (!current_target_set)
  {
    current_target_set = true;
    current_target_xyz.x = get_real_coord_x(current_node->x);
    current_target_xyz.y = get_real_coord_y(current_node->y);
  }

  nav_msgs::Path path;
  nav_msgs::Path wrong_way_path;

  path.header.stamp = ros::Time::now();
  path.header.frame_id = "map";

  pcl::PointCloud<pcl::PointXYZ> out_points;
  current_path = out_points;

  int count = 0;
  geometry_msgs::Point point_before;
  point_before.x = current_node->x;
  point_before.y = current_node->y;

  while (current_node->parent != start_node)
  {
    out_points.points.push_back(pcl::PointXYZ(get_real_coord_x(current_node->x), get_real_coord_y(current_node->y), 0));
    geometry_msgs::PoseStamped pose;

    pose.pose.position.x = get_real_coord_x(current_node->x);
    pose.pose.position.y = get_real_coord_y(current_node->y);
    pose.pose.position.z = 0;

    tf::Quaternion q;
    tfScalar angle = (point_before.y - current_node->y) / sqrt((point_before.x - current_node->x) * (point_before.x - current_node->x) + (point_before.y - current_node->y) * (point_before.y - current_node->y));
    q.setEulerZYX(angle, 0, 0);

    geometry_msgs::Quaternion quaternion;

    quaternion.x = q.getX();
    quaternion.y = q.getY();
    quaternion.z = q.getZ();
    quaternion.w = q.getW();
    point_before.x = current_node->x;
    point_before.y = current_node->y;

    pose.pose.orientation = quaternion;

    pose.header.stamp = ros::Time::now();
    pose.header.frame_id = "map";
    wrong_way_path.poses.push_back(pose);
    current_node = current_node->parent;
    count++;
  }

  for (int i = count - 1; i > 0; i--)
  {
    path.poses.push_back(wrong_way_path.poses[i]);
    current_path.push_back(out_points[i]);
  }

  path_msg.publish(path);
  sensor_msgs::PointCloud2 path_msg;

  pcl::toROSMsg(out_points, path_msg);
  path_msg.header.frame_id = "map";
  path_msg.header.stamp = ros::Time::now();
  path_odom.publish(path_msg);
}

int Pathfinder::determine_direction(int x, int y)
{

  int direction;
  if (x == -1 && y == 0)
  {
    direction = 180;
  }

  if (x == -1 && y == 1)
  {
    direction = 135;
  }
  if (x == 0 && y == 1)
  {
    direction = 90;
  }
  if (x == 1 && y == 1)
  {
    direction = 45;
  }
  if (x == 1 && y == 0)
  {
    direction = 0;
  }
  if (x == 1 && y == -1)
  {
    direction = -45;
  }
  if (x == 0 && y == -1)
  {
    direction = -90;
  }
  if (x == -1 && y == -1)
  {
    direction = -135;
  }

  return direction;
}

float Pathfinder::direction_price(int parent_direction, int neigbour_direction)
{
  if (parent_direction == -1)
  {
    return 0;
  }
  int difference = abs(parent_direction - neigbour_direction);
  if (difference > 180)
  {
    difference = 360 - difference;
  }
  return 0;
}

bool Pathfinder::node_proximity_check(const a_star_node *current_node, a_star_node **nodes)
{
  bool check = false;
  for (int i = -1; i < 2; i++)
  {
    for (int j = -1; j < 2; j++)
    {
      if (current_node->x + i >= 0 && current_node->x + i < num_of_grid_cells && current_node->y + j >= 0 && current_node->y + j < num_of_grid_cells)
      {
        if (grid[current_node->x + i][current_node->y + j].num_of_points == 0 && !grid[current_node->x + i][current_node->y + j].possible_to_traverse)
        {
          check = true;
        }
      }
    }
  }
  return check;
}

bool Pathfinder::check_for_target_proximity()
{
  // Returns true if robot is at target
  geometry_msgs::Point target;

  if (current_target_set)
  {
    target = current_target_xyz;
  }
  else
  {
    target = global_target_xyz;
  }

  float sum = sqrt((target.x - robot_x) * (target.x - robot_x) + (target.y - robot_y) * (target.y - robot_y));
  printf("%f\n",sum);
  printf("%d\n",current_target_set);

  if (sum < 0.3)
  {
    pcl::PointCloud<pcl::PointXYZ> empty_path;
    current_path = empty_path;
    current_target_set = false;
    global_target_set = false;
    printf("prebehlo\n");

    nav_msgs::Path path;
    path.header.stamp = ros::Time::now();
    path.header.frame_id = "map";
    path_msg.publish(path);
    return true;

  }
  //Temp target
  if (sum < 2.0)
  {
    if (current_target_set)
    {
      return true;
    }
  }
  //GLobal target
  

  return false;

}

int Pathfinder::get_grid_coord_x(float x)
{
  return floor((x - Map_centre_x) / grid_size) + num_of_grid_cells / 2;
};

int Pathfinder::get_grid_coord_y(float y)
{
  return floor((y - Map_centre_y) / grid_size) + num_of_grid_cells / 2;
};

float Pathfinder::get_real_coord_x(int x)
{
  return (x - num_of_grid_cells / 2) * grid_size + Map_centre_x + grid_size / 2;
};

float Pathfinder::get_real_coord_y(int y)
{
  return (y - num_of_grid_cells / 2) * grid_size + Map_centre_y + grid_size / 2;
};