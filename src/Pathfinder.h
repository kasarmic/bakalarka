#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "nav_msgs/Odometry.h"
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include "sensor_msgs/NavSatFix.h"
#include "geometry_msgs/Point.h"
#include "nav_msgs/Path.h"

class MAPPER;
class Grid_cell;

class Pathfinder
  {
  public:
    struct a_star_node
    {
      bool obstacle = false;
      bool visited = false;
      float GlobalGoal = INFINITY;
      float LocalGoal = INFINITY;
      int cell_value_for_pathfinding;
      int parent_direction;
      int x;
      int y;
      float x_a;
      float y_a;
      a_star_node *parent = nullptr;
    };

    void setup_pathfinder(int num_of_cells, ros::NodeHandle &nodehandle);
    void callback_target_gps(const sensor_msgs::NavSatFixConstPtr &msg);
    void callback_current_gps(const sensor_msgs::NavSatFixConstPtr &msg);
    geometry_msgs::Point GPS_to_ENU(double latitude, double longitude, double altitude);
    void parse_parameters(Grid_cell **map, float robot_position_x, float robot_position_y, float grid_len, float centre_x, float centre_y);
    void take_care_of_path();
    bool check_path();
    void find_path();
    int determine_direction(int x, int y);
    float direction_price(int parent_direction, int neigbour_direction);
    bool node_proximity_check(const a_star_node *current_node, a_star_node **nodes);
    bool check_for_target_proximity();
    int get_grid_coord_x(float x);
    int get_grid_coord_y(float y);
    float get_real_coord_x(int x);
    float get_real_coord_y(int y);
    int num_of_grid_cells;
    float grid_size;
    sensor_msgs::NavSatFix current_position;
    sensor_msgs::NavSatFix start_position;
    sensor_msgs::NavSatFix global_target;
    geometry_msgs::Point current_position_xyz;
    geometry_msgs::Point global_target_xyz;
    geometry_msgs::Point current_target_xyz;
    bool current_target_set;
    bool global_target_set;
    bool find_new_path;
    geometry_msgs::Point start_position_ECEF;

    ros::Subscriber sub_target_gps;
    ros::Subscriber sub_current_gps;
    ros::Publisher path_odom;
    ros::Publisher path_msg;
    bool start;
    double robot_x;
    double robot_y;
    float Map_centre_x;
    float Map_centre_y;
    ros::Time time_of_last_pathfinding;

    pcl::PointCloud<pcl::PointXYZ> current_path;

    a_star_node **nodes;
    Grid_cell **grid;

  private:
  };

#endif
