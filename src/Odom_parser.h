#ifndef ODOM_PARSER_H
#define ODOM_PARSER_H
#include <ros/ros.h>
#include "nav_msgs/Odometry.h"
#include <tf/transform_broadcaster.h>

class Odom_parser
{
public:
    void SetParams();
    void callback_publish_odom(const nav_msgs::Odometry::ConstPtr &msg);
    void publish_odom();
    bool use_odometry;

private:
    ros::NodeHandle nodeHandle;
    ros::Subscriber odom_sub;
    tf::TransformBroadcaster tf_broadcaster;
    ros::Time last_publish;
    std::string odom_frame;
    std::string robot_frame;
    std::string odom_data;
};

#endif