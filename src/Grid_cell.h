#ifndef GRID_CELL_H
#define GRID_CELL_H

#include "geometry_msgs/Point.h"


class Grid_cell
{
public:
    void setup_grid(int points_per_grid);
    void clear_cell();

    bool new_points;
    geometry_msgs::Point **points;
    bool is_traversable;
    bool is_curb;
    int num_of_points;
    int max_points;
    float lowest_point_height;
    float average_height;
    bool interpolated;
    bool possible_to_traverse;
    int cell_value_for_pathfinding;

private:
};

#endif