#include "Path_follower.h"

/**
 * Setups Path_follower to basic configuration
 */
void Path_follower::setup_Path_follower()
{
    nodeHandle.param<float>("/Path_follower/l", l, 0.75);
    nodeHandle.param<float>("/Path_follower/forward_speed", forward_speed, 0.4);
    path_sub = nodeHandle.subscribe("/nav_msg_path", 1, &Path_follower::callback_path, this);
    odom_sub = nodeHandle.subscribe("/icp_odom", 1, &Path_follower::callback_odom, this);
    cmd_vel_pub = nodeHandle.advertise<geometry_msgs::Twist>("cmd_vel", 1000);
    current_position.x = 0;
    current_position.y = 0;
    current_position.yaw = 0;
}

/**
 * Updates robot position.
 */
void Path_follower::callback_odom(const nav_msgs::Odometry::ConstPtr &msg)
{
    current_position.x = msg->pose.pose.position.x;
    current_position.y = msg->pose.pose.position.y;

    tf::Quaternion q(msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);
    current_position.yaw = yaw;
    compute_cmd_vel();
}

/**
 * Updates path from Pathfinder.
 */
void Path_follower::callback_path(const nav_msgs::Path &path)
{
    current_path = nav_msgs::Path(path);
    compute_cmd_vel();
}

/**
 * Computes cmd_vel velocieties based on robot's position from path.
 */
void Path_follower::compute_cmd_vel()
{
    if (current_path.poses.size() != 0)
    {
        pcl::PointCloud<pcl::PointXYZ> transformed_points;
        int index = -1;
        float distance_treshold = INFINITY;
        for (int i = 0; i < current_path.poses.size(); i++)
        {
            float x = current_path.poses[i].pose.position.x - current_position.x;
            float y = current_path.poses[i].pose.position.y - current_position.y;
            float yaw = -current_position.yaw;
            float temp_x = x * cos(yaw) - y * sin(yaw);
            float temp_y = x * sin(yaw) + y * cos(yaw);
            float distance = compute_distance_from_circle(temp_x, temp_y);
            transformed_points.push_back(pcl::PointXYZ(temp_x, temp_y, distance));
            if (distance < distance_treshold && temp_x >= 0)
            {
                index = i;
                distance_treshold = distance;
            }
        }

        geometry_msgs::Twist msg;

        if (index != -1)
        {
            // we have point in front of robot
            pcl::PointXYZ point = transformed_points.points[index];
            float R = (l * l) / (2 * point.y);
            float omega = forward_speed / R;

            float angle = abs(atan2(point.y, point.x));

            if (abs(point.y) > 0.4 || angle > 40)
            {
                msg.linear.x = 0.1;
            }
            else
            {
                msg.linear.x = forward_speed;
            }
            msg.angular.z = omega;
        }
        else
        {
            pcl::PointXYZ point = transformed_points.points[0];
            float angle = atan2(point.y, point.x);
            if (angle >= 0)
            {
                msg.angular.z = 0.5;
            }
            else
            {
                msg.angular.z = -0.5;
            }
            // points are behind robot we need to turn him
        }
        printf("\nPUBLISHING \n x: %f twist: %f \n\n", msg.linear.x, msg.angular.z);
        cmd_vel_pub.publish(msg);
    }
}

/**
 * Compute's distance of point from circle around robot.
 * Robot is always centered at 0,0 coordinate.
 */
float Path_follower::compute_distance_from_circle(float x, float y)
{
    return abs(sqrt(x * x + y * y) - l);
}


/**
 * Startup of Path_follower object.
 */
int main(int argc, char **argv)
{
    ros::init(argc, argv, "PATH_FOLLOWER_NODE");
    Path_follower follower;
    follower.setup_Path_follower();
    ros::spin();

    return 0;
}