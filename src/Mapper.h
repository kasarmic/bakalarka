#ifndef MAPPER_H
#define MAPPER_H

#include "nav_msgs/Odometry.h"
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include "sensor_msgs/NavSatFix.h"
#include "geometry_msgs/Point.h"

class Pathfinder;
class Grid_cell;

class MAPPER
{
public:
  void SetParams(ros::NodeHandle &nodeHandle);
  void callback_odom(const nav_msgs::Odometry::ConstPtr &msg);
  void callback_points(const boost::shared_ptr<const sensor_msgs::PointCloud2> &input);
  void movemap();
  void copy_grid_cell(int x1, int y1, int x2, int y2);
  void integrate_PC2(pcl::PointCloud<pcl::PointXYZ>::Ptr pt_cloud);
  void insertionsort(Grid_cell *cell);
  void interpolate_empty_grid_cells();
  void determine_traversability();
  void safe_zone_check();
  int get_grid_coord_x(float x);
  int get_grid_coord_y(float y);
  float get_real_coord_x(int x);
  float get_real_coord_y(int y);
  void free_memory();

  ros::NodeHandle n_;
  Pathfinder *pathfinder;

private:
  float grid_size;

  ros::Publisher points_in_map;
  ros::Publisher traversability_pub;
  ros::Publisher interpolated_pub;
  ros::Publisher occupied_pub;
  ros::Publisher path_pub;
  ros::Publisher is_curb_pub;
  ros::Publisher possible_to_traverse_pub;
  ros::Publisher nav_msg_path;
  ros::Subscriber sub_points;
  ros::Subscriber sub_odom_slam;

  std::string odom_frame;
  bool use_odometry;
  float Map_centre_x;
  float Map_centre_y;
  float robot_position_x;
  float robot_position_y;
  int num_of_grid_cells;
  int points_per_grid;
  int pt_index;
  float point_to_point_distance_treshold;
  float max_height_treshold;
  float max_angle_treshold;
  float max_grid_cell_height;
  bool safe_zone_update;
  Grid_cell **grid;
  pcl::PointCloud<pcl::PointXYZ> FOUND_PATH;
};

#endif
