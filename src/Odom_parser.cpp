#include "Odom_parser.h"

void Odom_parser::SetParams()
{
    nodeHandle.param<bool>("/Odom_parser/use_odometry", use_odometry, false);
    nodeHandle.param<std::string>("/Odom_parser/odom_frame", odom_frame, "odom");
    nodeHandle.param<std::string>("/Odom_parser/robot_frame", robot_frame, "base_link");
    nodeHandle.param<std::string>("/Odom_parser/odom_data", odom_data, "-");
    last_publish = ros::Time::now();

    if (use_odometry)
    {
        odom_sub = nodeHandle.subscribe(odom_data, 1, &Odom_parser::callback_publish_odom, this);
    }
}

void Odom_parser::callback_publish_odom(const nav_msgs::Odometry::ConstPtr &msg)
{
    tf::Transform outgoing_odom;
    tf::Vector3 pose;
    tf::Quaternion q;

    pose.setX(msg->pose.pose.position.x);
    pose.setY(msg->pose.pose.position.y);
    pose.setZ(msg->pose.pose.position.z);
    q = tf::Quaternion(msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);

    outgoing_odom.setOrigin(pose);
    outgoing_odom.setRotation(q);

    if (ros::Time::now() > last_publish)
    {
        tf_broadcaster.sendTransform(tf::StampedTransform(outgoing_odom, ros::Time::now(), odom_frame, robot_frame));
    }
    last_publish = ros::Time::now();
}

void Odom_parser::publish_odom()
{
    tf::Transform outgoing_odom;
    tf::Vector3 pose;
    tf::Quaternion q;

    pose.setX(0);
    pose.setY(0);
    pose.setZ(0);
    q.setRPY(0, 0, 0);

    outgoing_odom.setOrigin(pose);
    outgoing_odom.setRotation(q);

    if (ros::Time::now() > last_publish)
    {
        tf_broadcaster.sendTransform(tf::StampedTransform(outgoing_odom, ros::Time::now(), odom_frame, robot_frame));
    }
    last_publish = ros::Time::now();
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "Odom_parser");

    Odom_parser parser;
    parser.SetParams();

    while (ros::ok())
    {
        if (!parser.use_odometry)
        {
            parser.publish_odom();
        }
        else
        {
            ros::spinOnce();
        }
    }

    return 0;
}
