#ifndef PATH_FOLLOWER_H
#define PATH_FOLLOWER_H

#include "nav_msgs/Odometry.h"
#include "nav_msgs/Path.h"
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <tf/tf.h>
#include <cmath>
#include <ros/ros.h>

class Path_follower
{
public:
    void setup_Path_follower();
    void callback_odom(const nav_msgs::Odometry::ConstPtr &msg);
    void callback_path(const nav_msgs::Path &path);
    void compute_cmd_vel();
    float compute_distance_from_circle(float x, float y);

private:
    struct Point_2D
    {
        float x;
        float y;
        float yaw;
    };
    Point_2D current_position;
    nav_msgs::Path current_path;
    float l;
    float forward_speed;
    ros::Publisher cmd_vel_pub;
    ros::Subscriber odom_sub;
    ros::Subscriber path_sub;
    ros::NodeHandle nodeHandle;
};

#endif