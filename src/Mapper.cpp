#include <ros/ros.h>
#include "nav_msgs/Odometry.h"
#include "nav_msgs/Path.h"
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include "geometry_msgs/Point.h"
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <cmath>
#include <iostream>
#include <list>
#include "Mapper.h"
#include "Grid_cell.h"
#include "Pathfinder.h"

/**
 * Setups parameters for Mapper and Pathfinder
 */
void MAPPER::SetParams(ros::NodeHandle &nodeHandle)
{
  nodeHandle.param<float>("/Mapper/grid_size", grid_size, 0.15);
  nodeHandle.param<int>("/Mapper/num_of_grid_cells", num_of_grid_cells, 200);
  nodeHandle.param<int>("/Mapper/points_per_grid", points_per_grid, 50);
  nodeHandle.param<float>("/Mapper/max_height_treshold", max_height_treshold, 0.5);
  nodeHandle.param<float>("/Mapper/max_angle_treshold", max_angle_treshold, 20.0);
  nodeHandle.param<float>("Mapper/max_grid_cell_height", max_grid_cell_height, 0.05);
  nodeHandle.param<float>("Mapper/point_to_point_distance", point_to_point_distance_treshold, 0.01);

  // Initialize grid map

  grid = new Grid_cell *[num_of_grid_cells];
  for (int i = 0; i < num_of_grid_cells; i++)
  {
    grid[i] = new Grid_cell[num_of_grid_cells];
  }

  for (int i = 0; i < num_of_grid_cells; i++)
  {
    for (int j = 0; j < num_of_grid_cells; j++)
    {
      grid[i][j].setup_grid(points_per_grid);
    }
  }

  pathfinder->setup_pathfinder(num_of_grid_cells, nodeHandle);

  is_curb_pub = nodeHandle.advertise<sensor_msgs::PointCloud2>("/is_curb", 1);
  traversability_pub = nodeHandle.advertise<sensor_msgs::PointCloud2>("/traversable", 1);
  possible_to_traverse_pub = nodeHandle.advertise<sensor_msgs::PointCloud2>("/possible_to_traverse", 1);
  path_pub = nodeHandle.advertise<sensor_msgs::PointCloud2>("/path", 1);
  nav_msg_path = nodeHandle.advertise<nav_msgs::Path>("/nav_msg_path", 1);
  sub_points = nodeHandle.subscribe("/map", 1, &MAPPER::callback_points, this);
  sub_odom_slam = nodeHandle.subscribe("/icp_odom", 1, &MAPPER::callback_odom, this);
  points_in_map = nodeHandle.advertise<sensor_msgs::PointCloud2>("/points_in_grid", 1);
  occupied_pub = nodeHandle.advertise<sensor_msgs::PointCloud2>("/occupied", 1);
  interpolated_pub = nodeHandle.advertise<sensor_msgs::PointCloud2>("/interpolated", 1);

  Map_centre_x = 0;
  Map_centre_y = 0;
  robot_position_x = 0;
  robot_position_y = 0;
}

/**
 * Odometry callback from SLAM
 */
void MAPPER::callback_odom(const nav_msgs::Odometry::ConstPtr &msg)
{
  robot_position_x = msg->pose.pose.position.x;
  robot_position_y = msg->pose.pose.position.y;
  pathfinder->robot_x = robot_position_x;
  pathfinder->robot_y = robot_position_y;
}

/**
 * Map callback from SLAM. Fucntion also publishes ros messages
 * and calls all necesary functions for map processing.
 */
void MAPPER::callback_points(const boost::shared_ptr<const sensor_msgs::PointCloud2> &input)
{
  pcl::PCLPointCloud2 pcl_pc2;
  pcl_conversions::toPCL(*input, pcl_pc2);
  pcl::PointCloud<pcl::PointXYZ>::Ptr pt_cloud(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromPCLPointCloud2(pcl_pc2, *pt_cloud);

  movemap();
  integrate_PC2(pt_cloud);
  interpolate_empty_grid_cells();
  determine_traversability();
  pathfinder->parse_parameters(grid, robot_position_x, robot_position_y, grid_size, Map_centre_x, Map_centre_y);
  pathfinder->take_care_of_path();

  pcl::PointCloud<pcl::PointXYZ> out_cloud;
  pcl::PointCloud<pcl::PointXYZ> out_cloud2;
  pcl::PointCloud<pcl::PointXYZ> out_cloud3;
  pcl::PointCloud<pcl::PointXYZ> out_cloud4;
  pcl::PointCloud<pcl::PointXYZ> out_cloud5;

  for (int i = 0; i < num_of_grid_cells; i++)
  {
    for (int j = 0; j < num_of_grid_cells; j++)
    {
      int pos_in_arr = grid[i][j].num_of_points;

      for (int k = 0; k < pos_in_arr; k++)
      {
        out_cloud.points.push_back(pcl::PointXYZ(grid[i][j].points[k]->x, grid[i][j].points[k]->y, grid[i][j].points[k]->z));
      }
      if (grid[i][j].interpolated)
      {
        out_cloud3.points.push_back(pcl::PointXYZ(get_real_coord_x(i), get_real_coord_y(j), grid[i][j].average_height));
      }
      else if (grid[i][j].num_of_points > 0 && !grid[i][j].interpolated)
      {
        out_cloud2.points.push_back(pcl::PointXYZ(get_real_coord_x(i), get_real_coord_y(j), grid[i][j].average_height));
      }
      if (grid[i][j].is_curb)
      {
        out_cloud4.points.push_back(pcl::PointXYZ(get_real_coord_x(i), get_real_coord_y(j), grid[i][j].average_height));
      }
      if (grid[i][j].possible_to_traverse)
      {
        out_cloud5.points.push_back(pcl::PointXYZ(get_real_coord_x(i), get_real_coord_y(j), grid[i][j].average_height));
      }
    }
  }

  sensor_msgs::PointCloud2 object_msg;
  sensor_msgs::PointCloud2 object_msg2;
  sensor_msgs::PointCloud2 object_msg3;
  sensor_msgs::PointCloud2 object_msg4;
  sensor_msgs::PointCloud2 object_msg5;

  pcl::toROSMsg(out_cloud, object_msg);
  object_msg.header.frame_id = "map";
  object_msg.header.stamp = ros::Time::now();
  points_in_map.publish(object_msg);

  pcl::toROSMsg(out_cloud2, object_msg2);
  object_msg2.header.frame_id = "map";
  object_msg2.header.stamp = ros::Time::now();
  occupied_pub.publish(object_msg2);

  pcl::toROSMsg(out_cloud3, object_msg3);
  object_msg3.header.frame_id = "map";
  object_msg3.header.stamp = ros::Time::now();
  interpolated_pub.publish(object_msg3);

  pcl::toROSMsg(out_cloud4, object_msg4);
  object_msg4.header.frame_id = "map";
  object_msg4.header.stamp = ros::Time::now();
  is_curb_pub.publish(object_msg4);

  pcl::toROSMsg(out_cloud5, object_msg5);
  object_msg5.header.frame_id = "map";
  object_msg5.header.stamp = ros::Time::now();
  traversability_pub.publish(object_msg5);
};

/**
 * Function moves centre of map to current robot position.
 * Also it coppies every grid_cell to its place with new middle of map.
 */
void MAPPER::movemap()
{
  int x_diff = floor((robot_position_x - Map_centre_x) / grid_size);
  int y_diff = floor((robot_position_y - Map_centre_y) / grid_size);
  // We have 4 cases where we copy grid cells in different way

  if (x_diff >= 0 && y_diff >= 0)
  {

    for (int x = 0; x < num_of_grid_cells; x++)
    {
      for (int y = 0; y < num_of_grid_cells; y++)
      {

        if (num_of_grid_cells - x_diff > x && num_of_grid_cells - y_diff > y)
        {
          copy_grid_cell(x, y, x + x_diff, y + y_diff);
        }
        else
        {
          grid[x][y].clear_cell();
        }
      }
    }
  }
  else if (x_diff >= 0 && y_diff < 0)
  {

    // Asi tu je nieco zle
    for (int x = 0; x < num_of_grid_cells; x++)
    {
      for (int y = num_of_grid_cells - 1; y >= 0; y--)
      {

        if (num_of_grid_cells - x_diff > x && num_of_grid_cells - y_diff < y)
        {
          copy_grid_cell(x, y, x + x_diff, y + y_diff);
        }
        else
        {
          grid[x][y].clear_cell();
        }
      }
    }
  }
  else if (x_diff < 0 && y_diff >= 0)
  {

    for (int x = num_of_grid_cells - 1; x >= 0; x--)
    {
      for (int y = 0; y < num_of_grid_cells; y++)
      {

        if (num_of_grid_cells - x_diff < x && num_of_grid_cells - y_diff > y)
        {
          copy_grid_cell(x, y, x + x_diff, y + y_diff);
        }
        else
        {
          grid[x][y].clear_cell();
        }
      }
    }
  }
  else if (x_diff < 0 && y_diff < 0)
  {

    for (int x = num_of_grid_cells - 1; x >= 0; x--)
    {
      for (int y = num_of_grid_cells - 1; y >= 0; y--)
      {

        if (num_of_grid_cells - x_diff < x && num_of_grid_cells - y_diff < y)
        {
          copy_grid_cell(x, y, x + x_diff, y + y_diff);
        }
        else
        {
          grid[x][y].clear_cell();
        }
      }
    }
  }
  Map_centre_x = get_real_coord_x(get_grid_coord_x(robot_position_x)) - grid_size / 2;
  Map_centre_y = get_real_coord_y(get_grid_coord_y(robot_position_y)) - grid_size / 2;
}

/**
 * Function copies contents of one grid cell to another.
 * @param x1 y1 represents coordinates of first cell in grid
 * @param x2 y2 represents coordinates of second cell in grid
 */
void MAPPER::copy_grid_cell(int x1, int y1, int x2, int y2)
{

  grid[x1][y1].lowest_point_height = grid[x2][y2].lowest_point_height;
  grid[x1][y1].max_points = grid[x2][y2].max_points;
  grid[x1][y1].new_points = grid[x2][y2].new_points;
  grid[x1][y1].is_traversable = grid[x2][y2].is_traversable;
  grid[x1][y1].num_of_points = grid[x2][y2].num_of_points;
  grid[x1][y1].average_height = grid[x2][y2].average_height;
  grid[x1][y1].interpolated = grid[x2][y2].interpolated;
  grid[x1][y1].is_curb = grid[x2][y2].is_curb;
  grid[x1][y1].possible_to_traverse = grid[x2][y2].possible_to_traverse;
  grid[x1][y1].cell_value_for_pathfinding = grid[x2][y2].cell_value_for_pathfinding;

  for (int i = 0; i < grid[x1][y1].num_of_points; i++)
  {
    grid[x1][y1].points[i]->x = grid[x2][y2].points[i]->x;
    grid[x1][y1].points[i]->y = grid[x2][y2].points[i]->y;
    grid[x1][y1].points[i]->z = grid[x2][y2].points[i]->z;
  }
}

/**
 * Function integrates pointcloud from SLAM into grid.
 * Points are sorted to grid cells based on they're coordinates.
 * Points in one grid cell are sorted by z coordinate.
 * Points that are too high in grid cell are filtered out
 * @param pt_cloud SLAM map pointcloud
 */
void MAPPER::integrate_PC2(pcl::PointCloud<pcl::PointXYZ>::Ptr pt_cloud)
{
  for (int i = 0; i < pt_cloud->points.size(); i++)
  {
    bool check = true;
    pcl::PointXYZ point = pt_cloud->points[i];
    int grid_x = get_grid_coord_x(point.x);
    int grid_y = get_grid_coord_y(point.y);

    if (grid_x >= 0 && grid_y >= 0 && grid_x < num_of_grid_cells && grid_y < num_of_grid_cells)
    {

      // Filtering out points that are too high
      if (point.z > grid[grid_x][grid_y].lowest_point_height + max_height_treshold)
      {
        check = false;
      }

      // Filtering out points that are too close to saved points in grid
      if (check)
      {
        for (int j = 0; j < grid[grid_x][grid_y].num_of_points; j++)
        {
          float x_p = grid[grid_x][grid_y].points[j]->x;
          float y_p = grid[grid_x][grid_y].points[j]->y;
          float z_p = grid[grid_x][grid_y].points[j]->z;
          float sum = sqrt((point.x - x_p) * (point.x - x_p) + (point.y - y_p) * (point.y - y_p) + (point.z - z_p) * (point.z - z_p));

          if (sum < point_to_point_distance_treshold)
          {
            check = false;
          }
        }
      }

      if (check)
      {
        // add point to array
        int pos_in_arr = grid[grid_x][grid_y].num_of_points;
        if (pos_in_arr < points_per_grid)
        {
          grid[grid_x][grid_y].points[pos_in_arr]->x = point.x;
          grid[grid_x][grid_y].points[pos_in_arr]->y = point.y;
          grid[grid_x][grid_y].points[pos_in_arr]->z = point.z;
          grid[grid_x][grid_y].num_of_points++;
          grid[grid_x][grid_y].interpolated = false;

          if (pos_in_arr == 0)
          {
            grid[grid_x][grid_y].average_height = point.z;
          }
          else
          {
            grid[grid_x][grid_y].average_height = (grid[grid_x][grid_y].average_height * grid[grid_x][grid_y].num_of_points + point.z) / (grid[grid_x][grid_y].num_of_points + 1);
          }
          insertionsort(&grid[grid_x][grid_y]);

          if (point.z < grid[grid_x][grid_y].lowest_point_height)
          {
            // Delete points that are too high from lowest point
            grid[grid_x][grid_y].lowest_point_height = point.z;
            check = true;

            for (int iterator = 0; iterator < grid[grid_x][grid_y].num_of_points; iterator++)
            {
              if (check)
              {
                float temp_z = grid[grid_x][grid_y].points[iterator]->z;
                float lowest_point = grid[grid_x][grid_y].lowest_point_height;

                if (temp_z > lowest_point + max_height_treshold)
                {
                  grid[grid_x][grid_y].num_of_points = iterator;
                  break;
                }
              }
            }
            //Recompute average height
            float sum = 0;
            for (int iterator = 0; iterator < grid[grid_x][grid_y].num_of_points; iterator++)
            {
              sum += grid[grid_x][grid_y].points[iterator]->z;
            }

            grid[grid_x][grid_y].average_height = sum / grid[grid_x][grid_y].num_of_points;
          }
          grid[grid_x][grid_y].new_points = true;
        }
      }
    }
  }
};

/**
 * Insertion sort sorting alghorithm
 * Sorts points in one grid cell by they're height
 * @param cell grid cell that is going to be sorted
 */
void MAPPER::insertionsort(Grid_cell *cell)
{
  geometry_msgs::Point **array = cell->points;

  for (int k = 1; k < cell->num_of_points; k++)
  {
    geometry_msgs::Point temp;
    temp.x = array[k]->x;
    temp.y = array[k]->y;
    temp.z = array[k]->z;

    int j = k - 1;
    while (j >= 0 && temp.z <= array[j]->z)
    {
      array[j + 1]->x = array[j]->x;
      array[j + 1]->y = array[j]->y;
      array[j + 1]->z = array[j]->z;

      j = j - 1;
    }

    array[j + 1]->x = temp.x;
    array[j + 1]->y = temp.y;
    array[j + 1]->z = temp.z;
  }
};

/**
 * Function interpolates height of cell that doesn't have any points yet.
 * For it to interpolate there must be grid cells with points aroud particular cell.
 */
void MAPPER::interpolate_empty_grid_cells()
{

  int filter[5][5] = {{1, 4, 7, 4, 1}, {4, 16, 26, 16, 4}, {7, 26, 41, 26, 7}, {4, 16, 26, 16, 4}, {1, 4, 7, 4, 1}};
  int filterwidth = 2;

  // Compute interpolated average z coord for each grid cell

  for (int x = filterwidth; x < num_of_grid_cells - filterwidth; x++)
  {
    for (int y = filterwidth; y < num_of_grid_cells - filterwidth; y++)
    {

      if (grid[x][y].num_of_points == 0)
      {

        /*
        bool variables to chcek if there are grid cells
        with points around grid cell we are interpolating
        */
        bool left_up_corner_check = false;
        bool right_up_corner_check = false;
        bool left_down_corner_check = false;
        bool right_down_corner_check = false;

        float temp_height = 0;
        int normalize_const = 0;

        for (int i = -filterwidth; i <= filterwidth; i++)
        {
          for (int j = -filterwidth; j <= filterwidth; j++)
          {
            if (grid[x + i][y + j].num_of_points > 0)
            {

              if (i < 1 && j > 0)
              {
                left_up_corner_check = true;
              }
              else if (i > 0 && j > -1)
              {
                right_up_corner_check = true;
              }
              else if (i > -1 && j < 0)
              {
                right_down_corner_check = true;
              }
              else
              {
                left_down_corner_check = true;
              }
              temp_height += grid[x + i][y + j].average_height * filter[i + filterwidth][j + filterwidth];
              normalize_const += filter[i + filterwidth][j + filterwidth];
            }
          }
        }

        if (left_up_corner_check && right_up_corner_check && right_down_corner_check && left_down_corner_check)
        {
          grid[x][y].average_height = temp_height / normalize_const;
          grid[x][y].interpolated = true;
        }
      }
    }
  }
};

/**
 * Function determines traversability of each cell based on gradients between cells
 * and height difference between lowest and highest point in cell
 * It also creates buffer not traversable zone around each not traversable cell determined
 * by function. Cells around not traversable cell also get penalty cost for pathfinding.
 */
void MAPPER::determine_traversability()
{
  for (int x = 0; x < num_of_grid_cells; x++)
  {
    for (int y = 0; y < num_of_grid_cells; y++)
    {
      if (grid[x][y].num_of_points != 0)
      {

        float highest_point = grid[x][y].points[grid[x][y].num_of_points - 1]->z;
        if (highest_point - grid[x][y].lowest_point_height < max_grid_cell_height)
        {
          grid[x][y].is_curb = true;
        }
        else
        {
          grid[x][y].is_curb = false;
          grid[x][y].is_traversable = false;
        }
      }
      else
      {
        if (grid[x][y].interpolated)
        {
          grid[x][y].is_curb = true;
        }
        else
        {
          grid[x][y].is_curb = false;
          grid[x][y].is_traversable = false;
        }
      }
    }
  }

  float height_treshold = sin(max_angle_treshold * 3.14159 / 180) * grid_size;

  for (int x = 1; x < num_of_grid_cells - 1; x++)
  {
    for (int y = 1; y < num_of_grid_cells - 1; y++)
    {
      bool gradient_check = true;
      if (grid[x][y].is_curb)
      {
        for (int i = -1; i < 2; i++)
        {
          for (int j = -1; j < 2; j++)
          {
            float height_diff = abs(grid[x][y].average_height - grid[x + i][y + j].average_height);
            if (height_diff > height_treshold)
            {
              gradient_check = false;
            }
          }
        }
        if (gradient_check)
        {
          grid[x][y].is_traversable = true;
          grid[x][y].possible_to_traverse = true;
          grid[x][y].cell_value_for_pathfinding = 0;
        }
        else
        {
          grid[x][y].is_traversable = false;
          grid[x][y].possible_to_traverse = false;
        }
      }
      else
      {
        grid[x][y].is_traversable = false;
        grid[x][y].possible_to_traverse = false;
      }
    }
  }

  for (int x = 5; x < num_of_grid_cells - 5; x++)
  {
    for (int y = 5; y < num_of_grid_cells - 5; y++)
    {

      if (grid[x][y].is_traversable == false)
      {
        for (int i = -2; i < 3; i++)
        {
          for (int j = -2; j < 3; j++)
          {
            grid[x + i][y + j].possible_to_traverse = false;
          }
        }
        for (int i = -5; i < 6; i++)
        {
          for (int j = -5; j < 6; j++)
          {
            if (i == -5 || i == 5 || j == -5 || j == 5)
            {
              if (grid[x + i][y + j].cell_value_for_pathfinding < 1)
              {
                grid[x + i][y + j].cell_value_for_pathfinding = 1;
              }
            }
          }
        }
        for (int i = -4; i < 5; i++)
        {
          for (int j = -4; j < 5; j++)
          {
            if (i == -4 || i == 4 || j == -4 || j == 4)
            {
              if (grid[x + i][y + j].cell_value_for_pathfinding < 2)
              {
                grid[x + i][y + j].cell_value_for_pathfinding = 2;
              }
            }
          }
        }
        for (int i = -3; i < 4; i++)
        {
          for (int j = -3; j < 4; j++)
          {
            if (i == -3 || i == 3 || j == -3 || j == 3)
            {
              if (grid[x + i][y + j].cell_value_for_pathfinding < 3)
              {
                grid[x + i][y + j].cell_value_for_pathfinding = 3;
              }
            }
          }
        }
      }
    }
  }

  // This part of code just sets basic traversable startup area for robot.

  safe_zone_check();
  if (safe_zone_update)
  {
    int x_start = get_grid_coord_x(-2.50);
    int y_start = get_grid_coord_y(-2.50);
    ;
    int x_end = get_grid_coord_x(2.50);
    int y_end = get_grid_coord_y(2.50);
    if (x_start < 0)
    {
      x_start = 0;
    }
    if (y_start < 0)
    {
      y_start = 0;
    }

    if (x_end > num_of_grid_cells)
    {
      x_end = num_of_grid_cells;
    }

    if (y_end > num_of_grid_cells)
    {
      y_end = num_of_grid_cells;
    }

    for (int x = x_start; x < x_end; x++)
    {
      for (int y = y_start; y < y_end; y++)
      {
        grid[x][y].possible_to_traverse = true;
        if (grid[x][y].num_of_points == 0)
        {
          grid[x][y].average_height = -0.13;
        }
      }
    }
  }
}
/**
 * Function checks if robot has moved away from startup location.
 * If robot is away from startup location, traversability of start area
 * is not automatically set to true.
 */
void MAPPER::safe_zone_check()
{
  if (sqrt(robot_position_x * robot_position_x + robot_position_y * robot_position_y) > 10)
  {
    safe_zone_update = false;
  }
}

/**
 * Function converts real x coordinate to grid y coordinate
 * @param x real coordinate x
 * @return grid coordinate x
 */
int MAPPER::get_grid_coord_x(float x)
{
  return floor((x - Map_centre_x) / grid_size) + num_of_grid_cells / 2;
};

/**
 * Function converts real y coordinate to grid y coordinate
 * @param y real coordinate y
 * @return grid coordinate y
 */
int MAPPER::get_grid_coord_y(float y)
{
  return floor((y - Map_centre_y) / grid_size) + num_of_grid_cells / 2;
};

/**
 * Function converts grid x coordinate to real x coordinate.
 * @param x grid x coordinate
 * @return real x coordinate
 */
float MAPPER::get_real_coord_x(int x)
{
  return (x - num_of_grid_cells / 2) * grid_size + Map_centre_x + grid_size / 2;
};

/**
 * Function converts grid y coordinate to real y coordinate.
 * @param y grid y coordinate
 * @return real y coordinate
 */
float MAPPER::get_real_coord_y(int y)
{
  return (y - num_of_grid_cells / 2) * grid_size + Map_centre_y + grid_size / 2;
};

/**
 * Function free's manually alocated memory.
 */
void MAPPER::free_memory()
{
  for (int i = 0; i < num_of_grid_cells; i++)
  {
    for (int j = 0; j < num_of_grid_cells; j++)
    {
      for (int k = 0; k < points_per_grid; k++)
      {
        delete grid[i][j].points[k];
      }
      delete[] grid[i][j].points;
    }
  }

  for (int i = 0; i < num_of_grid_cells; i++)
  {
    delete[] grid[i];
  }
  delete[] grid;
}

/**
 * Startup of MAPPER and Pathfinder objects
 */
int main(int argc, char **argv)
{
  ros::init(argc, argv, "MAPPER_NODE");
  MAPPER map;
  Pathfinder pathfinder;
  map.pathfinder = &pathfinder;
  map.SetParams(map.n_);
  ros::spin();
  map.free_memory();

  return 0;
}
