#include "Grid_cell.h"
#include <cmath>

/**
 * Function setups basic grid cell configuration
 * @param points_per_grid maximum points to be stored in cell
 */
void Grid_cell::setup_grid(int points_per_grid)
{
    num_of_points = 0;
    cell_value_for_pathfinding = 0;
    max_points = points_per_grid;
    new_points = false;
    is_traversable = false;
    interpolated = false;
    is_curb = false;
    possible_to_traverse = false;
    lowest_point_height = INFINITY;
    average_height = INFINITY;
    points = new geometry_msgs::Point *[max_points];
    for (int i = 0; i < max_points; i++)
    {
        points[i] = new geometry_msgs::Point;
    }
}

/**
 * Function resets grid cell to its basic configuration
 */
void Grid_cell::clear_cell()
{
    num_of_points = 0;
    new_points = false;
    is_traversable = false;
    lowest_point_height = INFINITY;
    average_height = INFINITY;
    interpolated = false;
    is_curb = false;
    possible_to_traverse = false;
    cell_value_for_pathfinding = 0;
}
